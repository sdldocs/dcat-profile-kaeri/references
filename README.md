# KAERI의 BigData의 Data Catalog 구축

원자력발전소 상태 데이터 세트 제공을 하기 위한 데이터 카탈로그 시스템을 구축에 필요한 reference collection이다.

이는 크게 부분으로 나뉜다.
- **DCAT(Data Catalog)**. 데이터 세트의 메타 데이터를 표현하는 카탈로그 표준 관련
- **Semantic Web**
    - **RDF(Resource Description Language)**. DCAT을 표현하는 languages에 관련
    - **OWL(Web Ontology Language)** 과 **SKOS(Simple Knowledge Organization System)**. RDF를 이용하여 Ontolgy 또는 지식을 표현 관련 (Semantic Web 기술)
    - **SPARQL(SPARQL Protocol and RDF Query Language)**. RDF (Resource Description Framework) 형식으로 저장된 데이터에서 정보를 조회하고 검색
    