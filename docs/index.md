DCAT Profile 구축을 위한 Reference Index이다.

DCAT:

- [DCAT](https://sdldocs.gitlab.io/dcat/)

Semantic WEb:

- [RDF](https://sdldocs.gitlab.io/rdf/) 
- [SKOS Primer](https://sdldocs.gitlab.io/skos-primer/)
- [OWL Primer](https://sdldocs.gitlab.io/owl-primer/)
- [SPARQL](https://sdldocs.gitlab.io/sparql/)

Other Refs related with Big Data:

- [데이터 품질](https://sdldocs.gitlab.io/data-quality/references/data-quality/)
- [데이터 프로파일링](https://sdldocs.gitlab.io/data-quality/references/data-profiling/)
- [데이터 검증](https://sdldocs.gitlab.io/data-quality/references/data-validation/)
- [데이터 무결성](https://sdldocs.gitlab.io/data-quality/references/data-integrity/)
- [데이터 파이프라인](https://sdldocs.gitlab.io/data-quality/references/data-pipeline/)

- [ToResearch](to-research.md)
