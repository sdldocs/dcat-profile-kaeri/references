## [CKAN](https://ckan.org/)
CKAN is an open-source DMS (data management system) for powering data hubs and data portals. CKAN makes it easy to publish, share and use data. It powers hundreds of data portals worldwide.

- [\[CKAN 알아보기 1부\] 질문과 답으로 알아본 오픈소스 데이터 포털 플랫폼, CKAN](https://dataonair.or.kr/home/%eb%8d%b0%ec%9d%b4%ed%84%b0-%ea%b4%91%ec%9e%a5/%eb%8d%b0%ec%9d%b4%ed%84%b0-%ec%9d%b8%ec%82%ac%ec%9d%b4%ed%8a%b8/%eb%8d%b0%ec%9d%b4%ed%84%b0-%ec%a7%80%ec%8b%9d%ea%b3%b5%ec%9c%a0/?mod=document&pageid=1&keyword=CKAN&uid=124)
- [\[CKAN 알아보기 2부\] “CKAN은 기술보다 활용에 초점을 맞춰야”](https://dataonair.or.kr/home/%eb%8d%b0%ec%9d%b4%ed%84%b0-%ea%b4%91%ec%9e%a5/%eb%8d%b0%ec%9d%b4%ed%84%b0-%ec%9d%b8%ec%82%ac%ec%9d%b4%ed%8a%b8/%eb%8d%b0%ec%9d%b4%ed%84%b0-%ec%a7%80%ec%8b%9d%ea%b3%b5%ec%9c%a0/?mod=document&pageid=1&keyword=CKAN&uid=125)
- [\[CKAN 알아보기 3부\] “CKAN은 기술보다 활용에 초점을 맞춰야”](https://dataonair.or.kr/home/%eb%8d%b0%ec%9d%b4%ed%84%b0-%ea%b4%91%ec%9e%a5/%eb%8d%b0%ec%9d%b4%ed%84%b0-%ec%9d%b8%ec%82%ac%ec%9d%b4%ed%8a%b8/%eb%8d%b0%ec%9d%b4%ed%84%b0-%ec%a7%80%ec%8b%9d%ea%b3%b5%ec%9c%a0/?mod=document&pageid=1&keyword=CKAN&uid=129)

## DCAT-AP
DCAT-AP에 대한 자료 모음
#### [Towards an open government data ecosystem in Europe using common standards](https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/document/towards-open-government-data-ecosystem-europe-using-common-standards)
- DCAT-AP는 DCAT Appication profile for data portals in Europe의 약자이다. 이는 DCAT의 확장으로 EU 내의 EU, 중앙정부 또는 지방정부가 운영하는 data portal을 연결하여 아래와 같은 데이터 사용 환경을 사용자에게 제공하기 위한 것이다. 
    1. 데이터 소비자는 EU 회원국, 다른 포털 및 다른 기관의 다른 데이터 포털들을 single point of access와 교차 검색를 통해 데이터세트 검색 
    2. 이를 위한 다른 형식으로 기술된 메타데이터에 대하여 포털을 공통 메타데이터 언어로 변환하여 표준을 준수할 수 있도록 한다.

- 주요한 도전과제는 다음과 같다.
    - Different metadata specifications, i.e. different semantics:
    - Heterogeneous data formats
    - Different quality of the published data
    - Various non-interoperable technologies, software platforms and tools
    - 24 different languages
    - Various, or no, licences: 
    - Lack of awareness on both the publisher- and the user-side

즉, DCAT-AP는 우선 위에서 기술한 목표를 우선으로 개발을 시작하였다. 그러나 당시 DCAT(version 1.0)의 semantic을 확장도 필요하게 되었으며 이를 반영하였다. 따라서 이를 중심으로 EU의 회원국들은 각국의 환경에 따라 표준 사양을 확장하였다. 예를 들면, 네덜란드는 DCAT-AP-NL, 이탈리아는 DCAT-AP-IT 등이다. 

통계(statistical)와 지리공간(geospatial) 정보는 "EU 전역의 사용자로부터 수요가 가장 많은" 주제 범주 중 두 가지로 확인돠었다. 
지리공간과 통계 커뮤니티가 이미 자신의 영역과 관련된 개방형 데이터를 게시하는 데 큰 관심을 보이고 있었다. 그러나 이는 일반적으로 지리정보 또는 통계 전용포털을 통해 이루어졌으며, 다시 한 번 다양한 데이터 소스를 생성하고, 지리공간 포털, 통계 데이터베이스 또는 범용 개방형 데이터 포털에서 관련 데이터가 검색되었는지에 대해 크게 신경 쓰지 않는 최종 사용자에게 사용 가능한 개방형 데이터의 단편화를 추가한다. 지리적 데이터, 통계적 데이터 및 "일반" 개방형 데이터와 같이 서로 다른 영역 간의 격차를 해소하기 위해 의 두 가지 사양 StatDCAT-AP와 GeoDCAT-AP는 DCAT-AP 버전 1.1과 완전히 호환되는 방식으로 개발되었다.

이같은 역사적 사실에서 볼때 우리는 StatDCAT-AP 또는 GeoDCAT-AP 방법론을 따르는 것이 바람직할 것으로 보이며, 특히 연구 데이터를 위한 CERIF(https://eurocris.org/)와 Nuclear Monitoring를 위한 DCAT-eOS-AP를 참조하여야 할 것이다. 

DCAT-AP 구축을 위하여 사용된 open source management system인 CKAN(https://ckan.org/)에 대한 학습 또한 필요하다. 

따라서 점정적으로 "AER-DCAT-AP-KR"로 specification을 명명하여 수행한다. 그러나 아직 DCAT-AP-KR specification이 공식적으로 발표되지 않은 상태이므로 [Dcat-ap-kr을 위한 데이터포털 메타데이터 항목 조사](https://forum.datahub.kr/t/dcat-ap-kr/108)와 [DCAT-AP-KR: 국내 데이터 포털의 상호운용을 위한 애플리케이션 프로파일](http://journal.dcs.or.kr/xml/34808/34808.pdf)를 참고하도록 한다.

#### [클라우드 환경의 데이터 상호운용성 확보를위한 표준 및 표준 활용 기술 동향](http://weekly.tta.or.kr/weekly/files/20222919082950_weekly.pdf)

#### [EPOS DCAT-AP](https://github.com/epos-eu/EPOS-DCAT-AP) - an example of DCAT-AP
This project implements an extension of the DCAT-AP v 1.1 for the EPOS Research Infrastructure.

Main features of this profile are:

- Support for additional entities and resources such as: Organisation, Person, Facility, Publication, Software, Service, Project, Equipment, WebService
- Characterisation of APIs and Webservice for data access (using the hydra vocabulary www.hydra-cg.com/spec/latest/core/)
- Additional roles
- Support for annotations
- Support for validation and consistency check – via Shapes Constraint Language (SHACL, www.w3.org/TR/shacl)
- Support for data collections (i.e. Dataset dct:isPartOf, dct:hasPart)

#### [DCAT-AP IT - The Italian application profile for metadata of datasets](https://schema.gov.it/lode/extract?url=https://w3id.org/italia/onto/DCAT)

